package com.spring.testjava.application.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.spring.testjava.application.output.port.IPricePort;
import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PriceServiceTest {

  @InjectMocks
  private PriceService priceService;

  @Mock
  private IPricePort iPricePort;

  public static final long ID = 1;

  @Test
  @DisplayName("Test method return PriceModel")
  void getPriceReturnsPriceModel() {
    LocalDateTime date = LocalDateTime.now();
    List<PriceModel> priceModels = new ArrayList<>();
    PriceModel priceModel = PriceModel.builder().build();
    priceModels.add(priceModel);

    when(iPricePort.getPrices(date, ID, ID)).thenReturn(priceModels);

    PriceModel result = priceService.getPrice(date, ID, ID);

    assertEquals(priceModel, result);
    verify(iPricePort, times(1)).getPrices(date, ID, ID);
  }

  @Test
  @DisplayName("Test method return BusinessException PriceModels Is Empty")
  void getPriceThrowsBusinessExceptionWhenPriceModelsIsEmpty() {
    LocalDateTime date = LocalDateTime.now();
    List<PriceModel> priceModels = new ArrayList<>();

    when(iPricePort.getPrices(date, ID, ID)).thenReturn(priceModels);

    assertThrows(BusinessException.class, () -> priceService.getPrice(date, ID, ID));
    verify(iPricePort, times(1)).getPrices(date, ID, ID);
  }

  @Test
  @DisplayName("Test method return PriceModel With Highest Priority")
  void getPriceReturnsPriceModelWithHighestPriority() {
    LocalDateTime date = LocalDateTime.now();
    List<PriceModel> priceModels = new ArrayList<>();
    PriceModel priceModel1 = PriceModel.builder().build();
    priceModel1.setPriority(1);
    PriceModel priceModel2 = PriceModel.builder().build();
    priceModel2.setPriority(2);
    priceModels.add(priceModel1);
    priceModels.add(priceModel2);

    when(iPricePort.getPrices(date, ID, ID)).thenReturn(priceModels);

    PriceModel result = priceService.getPrice(date, ID, ID);

    assertEquals(priceModel2, result);
    verify(iPricePort, times(1)).getPrices(date, ID, ID);
  }

  @Test
  @DisplayName("Test method 2 return PriceModel")
  void getPrices2ReturnsPriceModel() {
    LocalDateTime date = LocalDateTime.now();
    PriceModel priceModel = PriceModel.builder().build();

    when(iPricePort.getPrices2(date, ID, ID)).thenReturn(priceModel);

    PriceModel result = priceService.getPrices2(date, ID, ID);

    assertEquals(priceModel, result);
    verify(iPricePort, times(1)).getPrices2(date, ID, ID);
  }
}