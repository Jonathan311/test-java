package com.spring.testjava.infrastructure.input.adapter.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.spring.testjava.application.input.port.IPriceUseCase;
import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.input.adapter.rest.bean.response.PriceDto;
import com.spring.testjava.infrastructure.input.adapter.rest.mapper.PriceMapper;
import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class PriceControllerTest {

  @InjectMocks
  private PriceController controller;

  @Mock
  private IPriceUseCase useCase;

  @Spy
  private PriceMapper mapper = Mappers.getMapper(PriceMapper.class);

  public static final long ID = 1;

  @Test
  @DisplayName("Test method return PriceDto")
  void getPriceApplyReturnsPriceDto() {
    LocalDateTime date = LocalDateTime.now();
    PriceModel priceModel = PriceModel.builder().build();
    PriceDto priceDto = new PriceDto();

    when(useCase.getPrice(date, ID, ID)).thenReturn(priceModel);
    when(mapper.toDto(priceModel, date)).thenReturn(priceDto);

    ResponseEntity<PriceDto> response = controller.getPriceApply(date, ID, ID);

    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(priceDto, response.getBody());
    verify(useCase, times(1)).getPrice(date, ID, ID);
    verify(mapper, times(1)).toDto(priceModel, date);
  }
}