package com.spring.testjava.infrastructure.output.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.exception.TechnicalException;
import com.spring.testjava.infrastructure.output.repository.PriceRepository;
import com.spring.testjava.infrastructure.output.repository.entity.PriceData;
import com.spring.testjava.infrastructure.output.repository.mapper.PriceDataMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PriceAdapterRepositoryTest {

  @Mock
  private PriceRepository repository;

  @Spy
  private PriceDataMapper mapper = Mappers.getMapper(PriceDataMapper.class);

  @InjectMocks
  private PriceAdapterRepository priceAdapterRepository;

  public static final long ID = 1;

  @Test
  @DisplayName("Test method return List Of Price Models When Repository Returns Data")
  void getPricesReturnsListOfPriceModelsWhenRepositoryReturnsData() {
    LocalDateTime date = LocalDateTime.now();
    List<PriceModel> expectedPrices = new ArrayList<>();
    when(
        repository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataId(
            date, date, ID, ID)).thenReturn(new ArrayList<>());

    List<PriceModel> prices = priceAdapterRepository.getPrices(date, ID, ID);

    assertNotNull(prices);
    assertEquals(expectedPrices.size(), prices.size());
    verify(repository,
        times(1)).findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataId(
        date, date, ID, ID);
    verify(mapper, times(0)).toDomain(any());
  }

  @Test
  @DisplayName("Test method return Throws TechnicalException When Repository ThrowsException")
  void getPricesThrowsTechnicalExceptionWhenRepositoryThrowsException() {
    LocalDateTime date = LocalDateTime.now();
    when(
        repository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataId(
            date, date, ID, ID)).thenThrow(new RuntimeException());

    assertThrows(TechnicalException.class, () ->
        priceAdapterRepository.getPrices(date, ID, ID)
    );
    verify(repository,
        times(1)).findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataId(
        date, date, ID, ID);
    verify(mapper, times(0)).toDomain(any());
  }

  @Test
  @DisplayName("Test method 2 return PriceModel When Repository Returns Data")
  void getPrices2ReturnsPriceModelWhenRepositoryReturnsData() {
    LocalDateTime date = LocalDateTime.now();
    PriceModel expectedPrice = PriceModel.builder().build();
    when(
        repository.findTopByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataIdOrderByPriorityDesc(
            date, date, ID, ID)).thenReturn(new PriceData());
    when(mapper.toDomain(any())).thenReturn(PriceModel.builder().build());

    PriceModel price = priceAdapterRepository.getPrices2(date, ID, ID);

    assertNotNull(price);
    assertEquals(expectedPrice, price);
    verify(repository, times(
        1)).findTopByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataIdOrderByPriorityDesc(
        date, date, ID, ID);
    verify(mapper, times(1)).toDomain(any());
  }

  @Test
  @DisplayName("Test method return Throws TechnicalException When Repository ThrowsException")
  void getPrices2ThrowsTechnicalExceptionWhenRepositoryThrowsException() {
    LocalDateTime date = LocalDateTime.now();
    when(
        repository.findTopByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataIdOrderByPriorityDesc(
            date, date, ID, ID)).thenThrow(new RuntimeException());

    assertThrows(TechnicalException.class, () -> priceAdapterRepository.getPrices2(date, ID, ID));
    verify(repository, times(
        1)).findTopByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataIdOrderByPriorityDesc(
        date, date, ID, ID);
    verify(mapper, times(0)).toDomain(any());
  }
}