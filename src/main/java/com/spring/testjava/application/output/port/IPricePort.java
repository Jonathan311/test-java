package com.spring.testjava.application.output.port;

import com.spring.testjava.domain.PriceModel;
import java.time.LocalDateTime;
import java.util.List;

public interface IPricePort {
  List<PriceModel> getPrices(LocalDateTime date, Long productId, Long brandId);
  PriceModel getPrices2(LocalDateTime date, Long productId, Long brandId);
}
