package com.spring.testjava.application.service;

import com.spring.testjava.application.input.port.IPriceUseCase;
import com.spring.testjava.application.output.port.IPricePort;
import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.exception.BusinessException;
import com.spring.testjava.infrastructure.exception.message.BusinessErrorMessage;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceService implements IPriceUseCase {

  @Autowired
  private IPricePort iPricePort;

  @Override
  public PriceModel getPrice(LocalDateTime date, Long productId, Long brandId) {
    List<PriceModel> priceModelApply = iPricePort.getPrices(date, productId, brandId);

    return priceModelApply.stream().max(Comparator.comparingInt(PriceModel::getPriority))
        .orElseThrow(() -> new BusinessException(BusinessErrorMessage.PRICE_APPLY_NOT_FOUND));
  }

  @Override
  public PriceModel getPrices2(LocalDateTime date, Long productId, Long brandId) {
    return iPricePort.getPrices2(date, productId, brandId);
  }
}
