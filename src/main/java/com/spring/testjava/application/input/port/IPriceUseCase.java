package com.spring.testjava.application.input.port;

import com.spring.testjava.domain.PriceModel;
import java.time.LocalDateTime;

public interface IPriceUseCase {
  PriceModel getPrice(LocalDateTime date, Long productId, Long brandId);
  PriceModel getPrices2(LocalDateTime date, Long productId, Long brandId);
}
