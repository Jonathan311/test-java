package com.spring.testjava.infrastructure.input.adapter.rest.mapper;

import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.input.adapter.rest.bean.response.PriceDto;
import java.time.LocalDateTime;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PriceMapper {

  PriceDto toDto(PriceModel priceModel, LocalDateTime date);

}
