package com.spring.testjava.infrastructure.input.adapter.rest;

import com.spring.testjava.application.input.port.IPriceUseCase;
import com.spring.testjava.domain.ErrorResponse;
import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.helper.Constants;
import com.spring.testjava.infrastructure.input.adapter.rest.bean.response.PriceDto;
import com.spring.testjava.infrastructure.input.adapter.rest.mapper.PriceMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = RequestMethod.GET)
@Tag(name = "Price Controller", description = "Controlador de precio a aplicar")
public class PriceController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private IPriceUseCase iPriceUseCase;

  @Autowired
  private PriceMapper mapper;

  @ApiResponse(responseCode = "200", description = "Success", content = {
      @Content(mediaType = "application/json", schema = @Schema(implementation = PriceDto.class))})
  @ApiResponse(responseCode = "400", description = "Bad Request", content = {
      @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))})
  @Operation(summary = "GetPriceApply", description = "Precio a aplicar")
  @GetMapping
  public ResponseEntity<PriceDto> getPriceApply(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date,
      @RequestParam Long productId, @RequestParam Long brandId) {
    logger.info("Start Controller: Date: [{}], ProductId: [{}], BrandId: [{}]", date, productId,
        brandId);
    PriceModel data = iPriceUseCase.getPrice(date, productId, brandId);
    logger.info("End Controller:[{}]", data);
    return ResponseEntity.ok().body(mapper.toDto(data, date));
  }
}
