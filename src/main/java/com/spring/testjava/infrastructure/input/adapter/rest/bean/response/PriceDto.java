package com.spring.testjava.infrastructure.input.adapter.rest.bean.response;

import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PriceDto {
  Long productId;
  Long brandId;
  LocalDateTime date;
  Integer priceList;
  Double price;
}
