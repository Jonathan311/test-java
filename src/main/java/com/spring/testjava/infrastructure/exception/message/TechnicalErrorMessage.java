package com.spring.testjava.infrastructure.exception.message;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TechnicalErrorMessage {
  SERVICE_NOT_FOUND("PRT000001", "Service not found"),
  UNEXPECTED_EXCEPTION("PRT000002", "Unexpected error"),
  PRICE_APPLY_EXCEPTION("PRT000003", "Error consultando precio a aplicar");


  private final String code;
  private final String message;
}
