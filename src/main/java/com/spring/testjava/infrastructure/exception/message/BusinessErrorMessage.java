package com.spring.testjava.infrastructure.exception.message;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BusinessErrorMessage {
  PRICE_APPLY_NOT_FOUND("PRB000001", "Precio a aplicar no encontrado");

  private final String code;
  private final String message;
}
