package com.spring.testjava.infrastructure.exception;

import com.spring.testjava.infrastructure.exception.message.BusinessErrorMessage;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BusinessException extends RuntimeException {

  final BusinessErrorMessage businessErrorMessage;
}
