package com.spring.testjava.infrastructure.helper;

public class Constants {

  private Constants() {}

  public static final String APP_SAFE = "${spring.application.services.safe-url}";

}
