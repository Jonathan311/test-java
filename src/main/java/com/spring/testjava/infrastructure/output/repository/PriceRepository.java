package com.spring.testjava.infrastructure.output.repository;

import com.spring.testjava.infrastructure.output.repository.entity.PriceData;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRepository extends JpaRepository<PriceData, Long> {

  List<PriceData> findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataId(
      LocalDateTime startDate, LocalDateTime endDate, Long productId, Long brandId);

  PriceData findTopByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataIdOrderByPriorityDesc(
      LocalDateTime startDate, LocalDateTime endDate, Long productId, Long brandId);
}
