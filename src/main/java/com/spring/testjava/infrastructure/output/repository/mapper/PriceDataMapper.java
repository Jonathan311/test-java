package com.spring.testjava.infrastructure.output.repository.mapper;

import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.output.repository.entity.PriceData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PriceDataMapper {

  @Mapping(source = "brandData.id", target = "brandId")
  PriceModel toDomain(PriceData priceData);

}
