package com.spring.testjava.infrastructure.output.adapter;

import com.spring.testjava.application.output.port.IPricePort;
import com.spring.testjava.domain.PriceModel;
import com.spring.testjava.infrastructure.exception.TechnicalException;
import com.spring.testjava.infrastructure.exception.message.TechnicalErrorMessage;
import com.spring.testjava.infrastructure.output.repository.PriceRepository;
import com.spring.testjava.infrastructure.output.repository.mapper.PriceDataMapper;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PriceAdapterRepository implements IPricePort {

  @Autowired
  private PriceRepository repository;

  @Autowired
  private PriceDataMapper mapper;

  @Override
  public List<PriceModel> getPrices(LocalDateTime date, Long productId, Long brandId) {
    try {
      return repository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataId(
          date, date, productId, brandId).stream().map(mapper::toDomain).toList();
    } catch (Exception e) {
      throw new TechnicalException(e, TechnicalErrorMessage.PRICE_APPLY_EXCEPTION);
    }
  }

  @Override
  public PriceModel getPrices2(LocalDateTime date, Long productId, Long brandId) {
    try {
      return mapper.toDomain(
          repository.findTopByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandDataIdOrderByPriorityDesc(
              date, date, productId, brandId));
    } catch (Exception e) {
      throw new TechnicalException(e, TechnicalErrorMessage.PRICE_APPLY_EXCEPTION);
    }
  }
}
